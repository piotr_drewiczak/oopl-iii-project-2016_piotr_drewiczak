package library;

public class Book {

	private String tittle;
	private String author;
	private Boolean Borrowed;
	public Book(String t,String a) {
		this.tittle=t;
		this.author=a;
		this.Borrowed=false;
	}
	public void InformationAboutBooks(){
		System.out.println("'"+this.tittle+"'"+" "+this.author+" "+"::(Borrowed: "+getBorrowed()+")");
	}
	public Boolean getBorrowed() {
		return Borrowed;
	}
	public void setBorrowed(Boolean borrowed) {
		Borrowed = borrowed;
	}
	
	public void AddBookToLibrary(Library library){
		library.AddBooks(this);
	}
	public String toString(){
		return this.tittle+" "+this.author;
	}
	
}
