package library;

public class Teacher extends Client{
	
	private final static int maxBorrowedBooks=5;
	
	public Teacher(Library library,String n, String s) {
	super(library,n,s);
	}

	@Override
	public void BorrowBook(Book book) {
		if(!book.getBorrowed()&&BorrowedBooks.size()<Teacher.maxBorrowedBooks){
			mapOfClientsAndTheirBooks.put(book,this);
			BorrowedBooks.add(book);
			book.setBorrowed(true);			
		}
		else if(BorrowedBooks.size()>=Teacher.maxBorrowedBooks){
			System.out.println("You have actually 5 books, firstly give back one to borrow another book");
		}
		else{
			System.out.println("You cannot borrow this book because it's actually borrowed.");
		}
		
	}
	
	@Override
	public void GiveBackBook(Book book) throws EmptyException {
		for (int i = 0; i < BorrowedBooks.size(); i++) 
		{
			if(BorrowedBooks.contains(book))
			{
				System.out.println("You gived back a book!");
				mapOfClientsAndTheirBooks.remove(book,this);
				book.setBorrowed(false);
				BorrowedBooks.remove(book);
				return;
			}
			else	{
				try {
					throw new EmptyException();
				} catch (EmptyException e) {
					e.getStackTrace();
				}
				
			}
			
		}
		}
}





