package library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;


public abstract class Person implements BorrowBooks,GiveBackBooks{
	protected String name;
	protected String surname;
	protected HashMap<Book,Person> mapOfClientsAndTheirBooks = new HashMap<Book,Person>();
	
	protected ArrayList<Book> BorrowedBooks = new ArrayList<Book>();
	public String toString(){
		return this.name+" "+this.surname;
	}
	public void MapInformation(){
		for(Entry<Book,Person > entry : mapOfClientsAndTheirBooks.entrySet()) {
			Book key = entry.getKey();
			Person value = entry.getValue();
		    System.out.println(value.toString()+" borrowed: "+key.toString());
		}
		
	}

}
