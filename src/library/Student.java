package library;

public class Student extends Client{

	private final static int maxBorrowedBooks=3;
	
	
	public Student(Library library,String n,String s) {
		super(library,n,s);
	}


	@Override
	public void BorrowBook(Book book) {
		if(!book.getBorrowed()&&BorrowedBooks.size()<Student.maxBorrowedBooks){
			BorrowedBooks.add(book);
			mapOfClientsAndTheirBooks.put(book,this);
			book.setBorrowed(true);
		}
		else if(BorrowedBooks.size()>=Student.maxBorrowedBooks){
			System.out.println("You have actually 3 books, firstly give back one to borrow another book");
		}
		else{
			System.out.println("You cannot borrow this book because it's actually borrowed.");
		}

	}
	
	@Override
	public void GiveBackBook(Book book) throws EmptyException {
		for (int i = 0; i < BorrowedBooks.size(); i++)
		{
			if(BorrowedBooks.contains(book)){
				mapOfClientsAndTheirBooks.remove(book,this);
				System.out.println("You gived back a book!");
				book.setBorrowed(false);
				BorrowedBooks.remove(book);
				return;
			}
			else{
				throw new EmptyException();
				
			}
		}
	}

}
