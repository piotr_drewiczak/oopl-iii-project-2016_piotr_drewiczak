package library;

public class Test {

	public static void main(String[] args) throws EmptyException {
		Library Library=new Library();
		
		Person T1= new Student(Library,"Dawid","Janiak");
		Person Y1= new Teacher(Library,"Piotr","Ibisz");
		
		Book B2=new Book("Czasomierze","David Mitchell");
		Book C2=new Book("Zmu� mnie","Lee Child");
		Book D2=new Book("Czarny mercedes","Janusz Majewski");
		Book E2=new Book("Setka","Krzysztof Varga");
		Book F2=new Book("Tolkien","Humprey Carpenter");
		Book G2=new Book("Delhi. Stolica ze z�ota i snu","Rana Dasgupta");
		Book H2=new Book("Osobliwe i cudowne przypadki Avy Lavender","Leslye Walton");
		Book J2=new Book("Peryferal","William Gibson");
		
		B2.AddBookToLibrary(Library);
		C2.AddBookToLibrary(Library);
		D2.AddBookToLibrary(Library);
		E2.AddBookToLibrary(Library);
		F2.AddBookToLibrary(Library);
		G2.AddBookToLibrary(Library);
		H2.AddBookToLibrary(Library);
		J2.AddBookToLibrary(Library);
	
		Library.PrintBooks();
		
		Y1.BorrowBook(J2);
		Y1.BorrowBook(D2);
		Y1.BorrowBook(J2);
		Y1.GiveBackBook(D2);
		Y1.GiveBackBook(D2);
		Y1.MapInformation();
	}

}
