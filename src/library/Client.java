package library;

public abstract class Client extends Person{

	
	public Client(Library library,String n,String s) {
		this.name=n;
		this.surname=s;
		library.AddClient(this);
	}
	
	public void InformationAboutClient(){
		System.out.println(this.name+""+this.surname);
	}
}
