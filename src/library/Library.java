package library;

import java.util.ArrayList;

public class Library {

	private ArrayList<Client> listOfClients = new ArrayList<Client>();
	private ArrayList<Book> listOfBooks = new ArrayList<Book>();
	
	public void AddBooks(Book book){
		this.listOfBooks.add(book);
	}
	
	public void AddClient(Client client){
		this.listOfClients.add(client);
	}
	
	public void PrintClients(){
	for (Client client : listOfClients) {
		client.InformationAboutClient();
		}
	}	
			

	public void PrintBooks(){
		for (Book book : listOfBooks) {
			book.InformationAboutBooks();
		}
		System.out.println();
	}
		
	}
	
	
	

